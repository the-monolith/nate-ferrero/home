import { component, React } from 'local/react/component'
import { Link } from 'local/react-ui/link'
import { Icon } from 'local/react-ui/icons'

export const HomeLink = component
  .props<{ className?: string }>()
  .render(({ className }) => (
    <Link to="/" className={className}>
      <Icon name="user-circle" />
      Nate
    </Link>
  ))
