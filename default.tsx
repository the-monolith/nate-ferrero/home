import { React, mount } from 'local/react/mount'
import { Root } from 'local/react/state'

import { router } from './router'

mount(<Root render={router} />)
