import { React } from 'local/react/component'
import { Router } from 'local/react/router'

import { HomeScene } from './scenes/home'

export const router = () => (
  <Router
    routes={{
      '/'() {
        return <HomeScene />
      }
    }}
  />
)
