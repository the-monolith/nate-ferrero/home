import { Style } from 'local/react/component'
import { defaultPageStyle, defaultBackdropStyle } from 'local/react-ui/page'
import { aliceBlue, gunmetal, tuftsBlue } from './theme'

export const backdropStyle: Style = {
  ...defaultBackdropStyle,
  backgroundColor: tuftsBlue,
  fontSize: '14px'
}

export const pageStyle: Style = {
  ...defaultPageStyle,
  backgroundColor: gunmetal,
  border: 'none',
  borderRadius: '0.125em',
  fontFamily: "'Noto Sans', sans-serif",
  lineHeight: '1.4',
  color: aliceBlue,
  boxShadow: `0 0.25em 0.5em ${gunmetal}`
}

export const headerStyle: Style = {
  margin: '0 0 1em'
}

export const textStyle: Style = {
  fontSize: '12pt'
}
