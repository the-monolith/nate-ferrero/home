import { component, React } from 'local/react/component'
import { stylesheet, title } from 'local/react/head'
import { Breadcrumb } from 'local/react-ui/breadcrumb'
import { Page } from 'local/react-ui/page'
import { Picture, defaultPictureStyle } from 'local/react-ui/picture'
import { Quote } from 'local/react-ui/quote'
import { Section } from 'local/react-ui/section'
import { HomeLink } from '../components/home-link'

import * as classes from './classes.css'

import { backdropStyle, pageStyle } from './resources/pageStyles'

import capeTown from './images/2018/cape-town.jpg'

export const TravelScene = component.render(() => {
  stylesheet('https://fonts.googleapis.com/css?family=Noto+Sans')
  title('Nate Ferrero - Travel')

  return (
    <Page backdropStyle={backdropStyle} style={pageStyle}>
      <Section>
        <HomeLink className={classes.homeLink} />
        <Breadcrumb>Travel</Breadcrumb>
      </Section>
      <Picture
        src={capeTown}
        style={{ ...defaultPictureStyle, width: '100%', height: '50vh' }}
      >
        That's me in Cape Town - I love seeing the earth and how amazing it is
        and how strong the rocks are that they can support a big boned dude like
        me 😄
      </Picture>
      <Section>
        <Quote author="Friedrich Nietzsche">
          Many are stubborn in pursuit of the path they have chosen, few in
          pursuit of the goal.
        </Quote>
      </Section>
    </Page>
  )
})
