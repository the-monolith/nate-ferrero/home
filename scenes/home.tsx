import { Address } from 'local/react-ui/address'
import { Badge, defaultBadgeStyle } from 'local/react-ui/badge'
import { Link } from 'local/react-ui/link'
import { Name } from 'local/react-ui/name'
import { Page } from 'local/react-ui/page'
import { Phone } from 'local/react-ui/phone'
import { Picture } from 'local/react-ui/picture'
import { Section } from 'local/react-ui/section'
import { Social } from 'local/react-ui/social'
import { Value } from 'local/react-ui/value'
import { Children, component, React, Style } from 'local/react/component'
import { stylesheet, title } from 'local/react/head'

import * as classes from './classes.css'
import profilePicture from './images/photo.jpg'
import { backdropStyle, pageStyle } from './resources/pageStyles'

const classificationBadgeStyle: Style = {
  ...defaultBadgeStyle,
  flexShrink: 0,
  width: '11.5em',
  justifyContent: 'center'
}

const badgeStyles: {
  topSecret: Style
  secret: Style
  confidential: Style
  public: Style
} = {
  topSecret: {
    ...classificationBadgeStyle,
    backgroundColor: '#000000'
  },
  secret: {
    ...classificationBadgeStyle,
    backgroundColor: '#990000'
  },
  confidential: {
    ...classificationBadgeStyle,
    backgroundColor: '#009400'
  },
  public: {
    ...classificationBadgeStyle,
    backgroundColor: '#0066CB'
  }
}

const paragraphStyle: Style = {
  display: 'flex',
  margin: '0.25em 0',
  lineHeight: '1.5'
}

const Paragraph = ({ children }: { children: Children }) => (
  <p style={paragraphStyle}>{children}</p>
)

export const HomeScene = component.render(() => {
  title('Nate Ferrero')
  stylesheet('https://fonts.googleapis.com/css?family=Noto+Sans')

  return (
    <Page
      backdropStyle={backdropStyle}
      style={pageStyle}
      className={classes.main}
    >
      <Section across center>
        <Picture src={profilePicture} className={classes.profilePhoto} />
        <Social
          facebook="nateferrero"
          twitter="NateFerrero"
          instagram="nateferrero"
          email="nateferrero@gmail.com"
          linkedIn="NateFerrero"
        />
      </Section>

      <Section>
        <Paragraph>
          <Badge>NAME</Badge>
          <Name
            first="Nathanael"
            preferred="Nate"
            middle="Stephen"
            last="Ferrero"
          />
        </Paragraph>
        <Paragraph>
          <Badge>CITY</Badge>
          <Address city="New York" state="NY" />
        </Paragraph>
        <Paragraph>
          <Badge>CALENDAR</Badge>
          <Link external to="https://youtu.be/czgOWmtGVGs">
            A New History for Humanity – The Human Era - Kurzgesagt
          </Link>
        </Paragraph>
        <Paragraph>
          <Badge>DATE OF BIRTH</Badge>11989-05-01
        </Paragraph>
      </Section>
      <Section>
        <Paragraph>
          <Badge>CATEGORY</Badge>Information classification
        </Paragraph>
        <Section indent>
          <Paragraph>
            <Badge style={badgeStyles.topSecret}>🔒&nbsp;TOP SECRET</Badge>
            Information that when shared outside of proper channels is
            reasonably expected to have a negative impact on an individual,
            society, or ecosystem.
          </Paragraph>
          <Paragraph>
            <Badge style={badgeStyles.secret}>🔒&nbsp;SECRET</Badge>Information
            that when shared outside of proper channels is reasonably suspected
            to have a negative impact on an individual, society, or ecosystem.
          </Paragraph>
          <Paragraph>
            <Badge style={badgeStyles.confidential}>🔒&nbsp;CONFIDENTIAL</Badge>
            Information that when shared outside of proper channels has a
            reasonable potential to have a negative impact on an individual,
            society, or ecosystem.
          </Paragraph>
          <Paragraph>
            <Badge style={badgeStyles.public}>🌎&nbsp;PUBLIC</Badge>Information
            that when widely shared is expected to have a neutral or positive
            impact on an individual, society, or ecosystem.
          </Paragraph>
        </Section>
      </Section>
      <Section>
        <Paragraph>
          <Badge>CATEGORY</Badge>Location
        </Paragraph>
        <Section indent>
          <Paragraph>
            <Badge>PLANET</Badge>Earth
          </Paragraph>
          <Paragraph>
            <Badge>COUNTRY</Badge>United States
          </Paragraph>
          <Paragraph>
            <Badge>STATE</Badge>Michigan
          </Paragraph>
          <Paragraph>
            <Badge>COUNTY</Badge>Washtenaw
          </Paragraph>
          <Paragraph>
            <Badge>CITY</Badge>Manchester
          </Paragraph>
          <Paragraph>
            <Badge>TOWNSHIP</Badge>Freedom
          </Paragraph>
        </Section>
      </Section>
      <Section>
        <Paragraph>
          <Badge>CATEGORY</Badge>Media
        </Paragraph>
        <Section indent>
          <Paragraph>
            <Badge>PROFILE</Badge>
            <Link external to="https://nateferrero.com/">
              NateFerrero.com
            </Link>
          </Paragraph>
          <Paragraph>
            <Badge>PERSONAL</Badge>
            <Link external to="https://www.instagram.com/nateferrero/">
              Instagram NateFerrero
            </Link>
          </Paragraph>
          <Paragraph>
            <Badge>POLITICAL</Badge>
            <Link external to="https://www.facebook.com/nateferrero">
              Facebook nateferrero
            </Link>
          </Paragraph>
          <Paragraph>
            <Badge>PERIODICAL</Badge>
            <Link external to="https://twitter.com/NateFerrero">
              Twitter NateFerrero
            </Link>
          </Paragraph>
          <Paragraph>
            <Badge>PROFESSIONAL</Badge>
            <Link external to="https://www.linkedin.com/in/NateFerrero">
              LinkedIn NateFerrero
            </Link>
          </Paragraph>
          <Paragraph>
            <Badge>PROPHETIC</Badge>
            <Link
              external
              to="https://transhumanism.fandom.com/wiki/User:NikolaiDybowskiFyodorov"
            >
              Fandom NikolaiDybowskiFyodorov
            </Link>
          </Paragraph>
        </Section>
      </Section>
      <Section>
        <Paragraph>
          <Badge>CATEGORY</Badge>Roles for interaction
        </Paragraph>
        <Section indent>
          <Paragraph>
            <Badge>FOUNDER</Badge>
            <span>
              <Link external to="https://1perfect.org/">
                1perfect
              </Link>
              Communication software
            </span>
          </Paragraph>
          <Paragraph>
            <Badge>FOUNDER</Badge>
            <span>
              <Link external to="https://ascension.network/">
                ascension.network
              </Link>
              Social network for improving life
            </span>
          </Paragraph>
          <Paragraph>
            <Badge>FOUNDER</Badge>
            <span>
              <Link external to="https://gitlab.com/the-monolith">
                GitLab the-monolith
              </Link>
              The one true piece of software
            </span>
          </Paragraph>
          <Paragraph>
            <Badge>SAAS</Badge>
            <span>
              <Link external to="https://flatfile.io/">
                flatfile
              </Link>
              Senior full-stack engineer
            </span>
          </Paragraph>
          <Paragraph>
            <Badge>MENTORSHIP</Badge>
            <span>
              <Link external to="https://www.codementor.io/@nateferrero">
                nateferrero@CodeMentor
              </Link>
              Software developer mentoring
            </span>
          </Paragraph>
          <Paragraph>
            <Badge>CONTRACTOR</Badge>
            <span>
              <Link external to="https://arc.dev/@nateferrero">
                nateferrero@arc
              </Link>
              React & Kotlin developer
            </span>
          </Paragraph>
          <Paragraph>
            <Badge>GAMING</Badge>
            <span>
              <Link
                external
                to="https://steamcommunity.com/id/volcanictortoise/"
              >
                volcanictortoise@Steam
              </Link>
              Player
            </span>
          </Paragraph>
          <Paragraph>
            <Badge>LIVESTREAM</Badge>
            <span>
              <Link external to="https://www.twitch.tv/toorealtobetrue">
                toorealtobetrue@Twitch
              </Link>
              Broadcaster
            </span>
          </Paragraph>
        </Section>
      </Section>
      <Section>
        <Paragraph>
          <Badge>CATEGORY</Badge>Short form communication
        </Paragraph>
        <Section indent>
          <Paragraph>
            <Badge>PHONE</Badge>
            <Phone countryCode="1" number="3232839179" />
          </Paragraph>
          <Paragraph>
            <Badge>PLATFORM</Badge>Text
          </Paragraph>
          <Paragraph>
            <Badge>PLATFORM</Badge>Voice mail
          </Paragraph>
          <Paragraph>
            <Badge>PLATFORM</Badge>
            <Link external to="https://www.whatsapp.com/">
              WhatsApp
            </Link>
          </Paragraph>
          <Paragraph>
            <Badge>PLATFORM</Badge>
            <Link external to="https://signal.org/">
              Signal
            </Link>
          </Paragraph>
        </Section>
      </Section>
      <Section>
        <Paragraph>
          <Badge>CATEGORY</Badge>Long form communication
        </Paragraph>
        <Section indent>
          <Paragraph>
            <Badge>CASUAL</Badge>
            <Value value="nateferrero@gmail.com" />
          </Paragraph>
          <Paragraph>
            <Badge>FINANCIAL</Badge>
            <Value value="nate.nyc@outlook.com" />
          </Paragraph>
          <Paragraph>
            <Badge>GAMING</Badge>
            <Value value="volcanictortoise@gmail.com" />
          </Paragraph>
          <Paragraph>
            <Badge>ORGANIZATION</Badge>
            <Value value="nathanael@ascension.network" />
          </Paragraph>
          <Paragraph>
            <Badge>ORGANIZATION</Badge>
            <Value value="nate@1perfect.org" />
          </Paragraph>
          <Paragraph>
            <Badge>ORGANIZATION</Badge>
            <Value value="nate@flatfile.io" />
          </Paragraph>
          <Paragraph>
            <Badge>ORGANIZATION</Badge>
            <Value value="nate@tagme.in" />
          </Paragraph>
        </Section>
      </Section>
      <Section>
        <Paragraph>
          <Badge>MISSION STATEMENT</Badge>
          <span>
            To possess deep understanding of{' '}
            <Link external to="https://kk.org/thetechnium/">
              The Technium
            </Link>
            , and identify improvements to be made that cause positive impact on
            an individual, society, or the world.
          </span>
        </Paragraph>
        <Paragraph>
          <Badge>VISION STATEMENT</Badge>
          <span>
            “Our task is to make nature, the blind force of nature, into an
            instrument of universal resuscitation and to become a union of
            immortal beings.” ―&nbsp;
            <Link
              external
              to="https://transhumanism.fandom.com/wiki/Nikolai_Fyodorovich_Fyodorov"
            >
              Nikolai&nbsp;F.&nbsp;Fyodorov
            </Link>
          </span>
        </Paragraph>
      </Section>
    </Page>
  )
})
